//
//  FirstViewController.swift
//  Giant Bomb
//
//  Created by Anders Tankred Holm on 05/11/15.
//  Copyright © 2015 Anders Tankred Holm. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LatestVideosViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var videosCollectionView: UICollectionView!
    
    @IBOutlet weak var featuredButton: UIButton!
    @IBOutlet weak var liveButton: UIButton!
    var videos = [VideoResult]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        videosCollectionView.delegate = self
        videosCollectionView.dataSource = self
        
        Alamofire.request(.GET, "https://cljdup9nr2.execute-api.us-east-1.amazonaws.com/prod/videos/", parameters: ["format": "json", "api_key" : "b8c36f8d8230472fbed158271632f56ceb214512", "limit" : "10"])
            .responseJSON { response in

                let videoResults = VideoResults(fromJson: JSON(response.result.value!))
                self.videos = videoResults.results
                
                dispatch_async(dispatch_get_main_queue()) {
                    self.videosCollectionView.reloadData()
                }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if let videoPlayerViewController = segue.destinationViewController as? VideoPlayerViewController, cell = sender as? VideoCell, indexPath = self.videosCollectionView?.indexPathForCell(cell) {
            let theme = videos[indexPath.item]
            videoPlayerViewController.video = theme
        }
        if let videoPlayerViewController = segue.destinationViewController as? VideoInfoViewController, cell = sender as? VideoCell, indexPath = self.videosCollectionView?.indexPathForCell(cell) {
            let video = videos[indexPath.item]
            videoPlayerViewController.video = video
        }
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCellWithReuseIdentifier("videoCell", forIndexPath: indexPath) as? VideoCell{
            
            let video = self.videos[indexPath.row]
            cell.configureCell(video)
            
            return cell
        }
        else {
            return VideoCell()
        }
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return videos.count
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        return CGSizeMake(500, 330)
    }
    
    func indexPathForPreferredFocusedViewInCollectionView(collectionView: UICollectionView) -> NSIndexPath? {
        return NSIndexPath(forItem: 0, inSection: 0)
    }
    
    func collectionView(collectionView: UICollectionView, canFocusItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView
    {
        
        let header = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: "latestHeader", forIndexPath: indexPath) as! LatestHeaderViewCollectionReusableView
        //header.headerTitle.text = collectionheader[indexPath.row]
        
        return header
    }
}

