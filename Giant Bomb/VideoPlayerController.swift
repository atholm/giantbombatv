//
//  VideoPlayerController.swift
//  Giant Bomb
//
//  Created by Anders Tankred Holm on 05/11/15.
//  Copyright © 2015 Anders Tankred Holm. All rights reserved.
//

import UIKit
import AVKit
import Alamofire
import SwiftyJSON

class VideoPlayerViewController: AVPlayerViewController {
    var video: VideoResult!
    var observer: AnyObject? = nil
    let channel = "6t4bites_aus"

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        var testURL = "";
        
        //// PLAY TWITCH
        /*
        Alamofire.request(.GET, "http://api.twitch.tv/api/channels/" + channel + "/access_token")
            .responseJSON { response in
                
                let authResult = JSON(response.result.value!)
                var token = authResult["token"].stringValue
                token = token.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())!

                let sig = authResult["sig"].stringValue
                
                testURL = "http://usher.twitch.tv/api/channel/hls/" + self.channel + ".m3u8?player=twitchweb&&token=" + token + "&sig=" + sig + "&allow_audio_only=true&allow_source=true&type=any&p=9333029"
                
                Alamofire.request(.GET, testURL)
                    .responseJSON { m3u_response in
                        let datastring = String(data: m3u_response.data!, encoding: NSUTF8StringEncoding)
                        let detector = try! NSDataDetector(types: NSTextCheckingType.Link.rawValue)
                        let matches = detector.matchesInString(datastring!, options: [], range: NSMakeRange(0, datastring!.characters.count))
                        
                        let url = (datastring! as NSString).substringWithRange(matches[0].range)
                        self.player = AVPlayer(URL: NSURL(string: url)!)
                        self.player?.play()
                }
        }
*/
        
        /// PLAY NORMAL
        
        let api_key = "&api_key=b8c36f8d8230472fbed158271632f56ceb214512"
        player = AVPlayer(URL: NSURL(string: video.hdPath + api_key)!)
        
        //if video.progress < video.lengthInSeconds {
        //    player?.seekToTime(CMTimeMakeWithSeconds(Double(video.progress), 1))
        //}
        
        player?.play()
        
        /*
        registerViewOn(video)
        let timer = CMTimeMake(1,1)
        observer = player?.addPeriodicTimeObserverForInterval(timer, queue: dispatch_get_main_queue(), usingBlock: { (time) -> Void in
            registerProgressOn(self.video, progress: Int(CMTimeGetSeconds(time)))
        })
        */
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        guard let observer = observer else {return}
        player?.removeTimeObserver(observer)
    }
    
}
