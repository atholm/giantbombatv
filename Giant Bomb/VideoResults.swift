//
//  VideoResults.swift
//  Giant Bomb
//
//  Created by Anders Tankred Holm on 05/11/15.
//  Copyright © 2015 Anders Tankred Holm. All rights reserved.
//

import Foundation
import SwiftyJSON

class VideoResults {
    
    var page : Int!
    var results : [VideoResult]!
    var totalPages : Int!
    var totalResults : Int!
    
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json == nil{
            return
        }
        page = json["offset"].intValue
        results = [VideoResult]()
        let resultsArray = json["results"].arrayValue
        for resultsJson in resultsArray{
            let value = VideoResult(fromJson: resultsJson)
            results.append(value)
        }
        totalPages = json["number_of_page_results"].intValue
        totalResults = json["number_of_total_results"].intValue
    }
    
}