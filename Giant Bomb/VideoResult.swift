//
//  VideoResult.swift
//  Giant Bomb
//
//  Created by Anders Tankred Holm on 05/11/15.
//  Copyright © 2015 Anders Tankred Holm. All rights reserved.
//

import Foundation
import SwiftyJSON

class VideoResult {
    
    var id : Int!
    var length : Int!
    var name : String!
    var deck : String!
    var publish_date : String!
    var imagePath : String!
    var hdPath : String!
    var video_type : String!
   
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json == nil{
            return
        }
        
        id = json["id"].intValue
        length = json["length_seconds"].intValue
        name = json["name"].stringValue
        deck = json["deck"].stringValue
        publish_date = json["publish_date"].stringValue
        imagePath = json["image"]["super_url"].stringValue
        hdPath = json["hd_url"].stringValue
        video_type = json["video_type"].stringValue
    }
    
}