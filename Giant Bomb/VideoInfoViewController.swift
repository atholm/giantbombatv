//
//  VideoInfoViewController.swift
//  Giant Bomb
//
//  Created by Anders Tankred Holm on 06/11/15.
//  Copyright © 2015 Anders Tankred Holm. All rights reserved.
//

import UIKit

class VideoInfoViewController: UIViewController {
    
    var video: VideoResult?
    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var videoImage: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        if let video = video
        {
            let url = NSURL(string: video.imagePath)!
            dispatch_async (dispatch_get_global_queue (DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                let data = NSData(contentsOfURL: url)!
                
                dispatch_async(dispatch_get_main_queue()) {
                    let img = UIImage(data: data)
                    self.backgroundImage.image = img
                    self.videoImage.image = img
               }
                
            }

            descriptionLabel.text = video.deck
            titleLabel.text = video.name
            let videoType = "Video Type: " + video.video_type
            let videoDate = " Publish Date: " + video.publish_date
            let videoLength = " Length: " + String(video.length)
            typeLabel.text = videoType + videoDate + videoLength
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if let videoPlayerViewController = segue.destinationViewController as? VideoPlayerViewController {
            videoPlayerViewController.video = video
        }
    }
}

