//
//  VideoCell.swift
//  Giant Bomb
//
//  Created by Anders Tankred Holm on 05/11/15.
//  Copyright © 2015 Anders Tankred Holm. All rights reserved.
//

import UIKit

class VideoCell: UICollectionViewCell {
    
    @IBOutlet weak var showImg: UIImageView!
    @IBOutlet weak var showLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        showImg.adjustsImageWhenAncestorFocused = true
        showImg.clipsToBounds = false
        showLbl.alpha = 0.0
    }
    
    func configureCell(tvShow: VideoResult) {
        
        if let title = tvShow.name {
            showLbl.text = title
        }
        
        
        if let path = tvShow.imagePath {
            let url = NSURL(string: path)!
            
            dispatch_async (dispatch_get_global_queue (DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                let data = NSData(contentsOfURL: url)!
                
                dispatch_async(dispatch_get_main_queue()) {
                    let img = UIImage(data: data)
                    self.showImg.image = img
                    //self.showImg.layer.cornerRadius = 5
                    //self.showImg.clipsToBounds = true
                }
                
            }
        }
    }
    
    override func didUpdateFocusInContext(context: UIFocusUpdateContext, withAnimationCoordinator coordinator: UIFocusAnimationCoordinator) {
        coordinator.addCoordinatedAnimations({ [unowned self] in
            if self.focused {
                self.showLbl.alpha = 1.0
            }
            else {
                self.showLbl.alpha = 0.0
            }
            }, completion: nil)
    }
}
